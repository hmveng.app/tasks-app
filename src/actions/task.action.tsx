import Task from '../models/task';

export enum TypeTasks {
    ADD = 'add task',
    REM0VE = 'remove task',
    CHANGE_DONE = 'change done task',
}

export type TaskAction = 
    | { type: TypeTasks.ADD, task: Task}
    | { type: TypeTasks.REM0VE, id: number}
    | { type: TypeTasks.CHANGE_DONE, id: number};