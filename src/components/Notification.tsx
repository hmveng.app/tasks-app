import Alert from 'react-bootstrap/Alert';

interface props {
    message: string,
    type: 'success' | 'danger' | 'warning'
}

export default function Notification({type = 'success', message}: props) {

    return (
        <>
            <Alert key={type} variant={type}>
                  {message}
            </Alert>
        </>
    )
}