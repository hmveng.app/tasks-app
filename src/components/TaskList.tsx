import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Stack from 'react-bootstrap/Stack';
import Form from 'react-bootstrap/Form';
import { BsXCircleFill } from "react-icons/bs";

import Task from '../models/task';
import TaskInfo from './TaskInfo';

interface props {
  tasks: Task[]
  onDeleteTask: Function
  onChangeDoneTask: Function
}

export default function TaskList({tasks, onDeleteTask, onChangeDoneTask}: props) {
  
    const countTasksDone = tasks.filter((item:Task) => item.done === true).length;

    return (
        <Stack gap={3}>

          <TaskInfo totalTask={tasks.length} taskDone={countTasksDone} />
          
          {tasks.map((task) => (
            <Row key={task.id} >
              <Col md="10">
                <Form.Check 
                  label={task.name}
                  defaultChecked={task.done}
                  onClick={() => onChangeDoneTask(task.id)} 
                />
              </Col>
              <Col md="2"> 
                <BsXCircleFill 
                  onClick={() => onDeleteTask(task.id)} 
                  style={{cursor:'pointer', color: 'red'}}
                />
              </Col>
            </Row>
          ))}
          
        </Stack>
    )
}