import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Stack from 'react-bootstrap/Stack';


interface props {
    onAddTask: Function
}

export default function AddTask({onAddTask}: props) {

    const [taskName, setTaskName] = useState('');
    const onClickHanlder = () => {
       if( taskName.trim().length === 0) {
         return;
       }

       onAddTask({
         name: taskName,
         done: false
       });

       setTaskName('');
    }

    return (
        <Stack direction="horizontal" gap={2} className="my-4">
            <Form.Control 
                className="me-auto" 
                value={taskName} 
                placeholder="Add your task here..." 
                onChange={(e) => setTaskName(e.target.value)}
            />
            <Button variant="dark" size="sm" onClick={onClickHanlder}>Add</Button>
        </Stack>
    )
}