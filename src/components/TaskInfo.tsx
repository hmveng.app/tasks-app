
interface props {
    totalTask: number,
    taskDone: number,
}

export default function TaskInfo({totalTask, taskDone}: props) {

    return (
        <>
            { totalTask <= 1 && (<div>There is {totalTask} task ({taskDone} done)</div>)}
            { totalTask > 1 && (<div>There are {totalTask} tasks ({taskDone} done)</div>)}
        </>
    )

    
  
}