import Task from '../models/task';
import { TaskAction } from '../actions/task.action';
import { TypeTasks } from '../actions/task.action';

interface State {
    tasks: Task[]
}

export const initialTasksState: State = {
    tasks: [
        {
            id: 1,
            name: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            done: true
        },
        {
            id: 2,
            name: 'Donec auctor neque in nulla accumsan, eget venenatis ex pharetra.',
            done: false
        },
        {
            id: 3,
            name: 'Phasellus ultricies, tellus non commodo aliquet, turpis orci pharetra dui',
            done: true
        },
        {
            id: 4,
            name: 'Vestibulum in ante leo. Ut quis ante eget justo varius viverra.',
            done: false
        },
        {
            id: 5,
            name: 'Vestibulum laoreet, velit a pharetra gravida, mi mi auctor justo',
            done: false
        }
    ]
}


export function taskReducer(state: State, action: TaskAction): State {
    switch(action.type) {
        case TypeTasks.ADD:
            return {
                tasks: [
                    ...state.tasks,
                    action.task
                ]
            }
        case TypeTasks.REM0VE:
           return {
                tasks: state.tasks.filter((item: Task) => item.id !== action.id)
           }
        case TypeTasks.CHANGE_DONE:
            return {
                 tasks: state.tasks.map((item: Task) => {
                    if(item.id == action.id) {
                       return {
                        ...item,
                        done: !item.done
                       }
                    }

                    return item
                 })
            }
        default:
            throw new Error("Unknown action");
    }

}