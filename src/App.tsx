import { useReducer, useState } from 'react';
import './App.scss';
import Container from 'react-bootstrap/Container';
import Stack from 'react-bootstrap/Stack';

import AddTask from './components/AddTask';
import Notification from './components/Notification';
import TaskList from './components/TaskList';

import { taskReducer, initialTasksState } from './reducers/task.reducer';
import { TypeTasks } from './actions/task.action';
import Task from './models/task';

function App() {
  const [state, dispatch] = useReducer(taskReducer, initialTasksState);
  const [notification, setNotification] = useState('');

  const onDeleteTaskHandler =  (taskId:number) => {
    dispatch({type: TypeTasks.REM0VE, id: taskId});
    setNotification('The task has been deleted.')
  };
  const onChangeTaskHandler =  (taskId:number) => {
    dispatch({type: TypeTasks.CHANGE_DONE, id: taskId});
    setNotification('The task has been updated.')
  };
  const onAddTaskHandler =  (task: Task) => {
    dispatch({type: TypeTasks.ADD, task: { ...task, id: state.tasks.length + 1}});
    setNotification('The task has been added.')
  }

  return (
    <>
      <Container fluid="md">
        
        <Stack direction="vertical" gap={2} className="col-md-8 mx-auto">
          <AddTask onAddTask={onAddTaskHandler}/>

          { notification.length > 0 && (<Notification message={notification} type='success' />) }

          <TaskList 
              tasks={state.tasks} 
              onDeleteTask={onDeleteTaskHandler}
              onChangeDoneTask={onChangeTaskHandler}
          />
        </Stack>
        
      </Container>
    </>
  );
}

export default App;
